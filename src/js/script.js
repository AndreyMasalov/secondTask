'use strict';

const container = document.getElementsByClassName('container');
const body = document.getElementsByClassName('page');
const btn = document.getElementsByClassName('nav__btn');

let shiny = {
    url: 'http://vue-tests.dev.creonit.ru/api/catalog/shiny',
    method: 'GET'
}

let diski = {
    url: 'http://vue-tests.dev.creonit.ru/api/catalog/diski',
    method: 'GET'
}

let httpGetAsync = (method, url, callback) => {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {

        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(xhr.responseText);
        }
        else if (xhr.status === 404) {
            console.log(responseText);
        }
    }
    xhr.open(method, url, true); // для асинхронного запроса
    xhr.send(null);
}

let toggleNode = () => {
    body[0].removeChild(container[0]);
    let con = document.createElement('div');
    con.setAttribute('class', 'container');
    body[0].appendChild(con);
}

let addData = (data) => {
    let objectData = JSON.parse(data);

    for (let i = 0; i < objectData.items.length; i++) {

        let main = document.createElement('div');
        main.setAttribute('class', 'article');
        container[0].appendChild(main);

        for (let key in objectData.items[i]) {

            if (key == 'title') {
                let par = document.createElement('p');
                par.setAttribute('class', 'article__title');
                let node = document.createTextNode('Название: ' + objectData.items[i][key]);
                par.appendChild(node);
                main.appendChild(par);
            }
            else if (key == 'price') {
                let par = document.createElement('p');
                par.setAttribute('class', 'article__price');
                let node = document.createTextNode('Цена: ' + objectData.items[i][key] + ' руб.');
                par.appendChild(node);
                main.appendChild(par);
            }
            else if (key == 'image') {
                let img = document.createElement('img');
                img.setAttribute('class', 'article__img');
                img.setAttribute('src', objectData.items[i][key]);
                main.appendChild(img);
            }
            else if (key == 'category') {
                let par = document.createElement('p');
                par.setAttribute('class', 'article__category');
                let node = document.createTextNode('Категория: ' + objectData.items[i][key].title);
                par.appendChild(node);
                main.appendChild(par);
            }
        }
    }
}

btn[0].addEventListener('click', () => {
    httpGetAsync(shiny.method, shiny.url, data => {
        toggleNode();
        addData(data);
    });
});

btn[1].addEventListener('click', () => {
    httpGetAsync(diski.method, diski.url, data => {
        toggleNode();
        addData(data);
    });
});