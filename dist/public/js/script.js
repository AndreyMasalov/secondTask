'use strict';

var container = document.getElementsByClassName('container');
var body = document.getElementsByClassName('page');
var btn = document.getElementsByClassName('nav__btn');

var shiny = {
    url: 'http://vue-tests.dev.creonit.ru/api/catalog/shiny',
    method: 'GET'
};

var diski = {
    url: 'http://vue-tests.dev.creonit.ru/api/catalog/diski',
    method: 'GET'
};

var httpGetAsync = function httpGetAsync(method, url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(xhr.responseText);
        } else if (xhr.status === 404) {
            console.log(responseText);
        }
    };
    xhr.open(method, url, true); // для асинхронного запроса
    xhr.send(null);
};

var toggleNode = function toggleNode() {
    body[0].removeChild(container[0]);
    var con = document.createElement('div');
    con.setAttribute('class', 'container');
    body[0].appendChild(con);
};

var addData = function addData(data) {
    var objectData = JSON.parse(data);

    for (var i = 0; i < objectData.items.length; i++) {

        var main = document.createElement('div');
        main.setAttribute('class', 'article');
        container[0].appendChild(main);

        for (var key in objectData.items[i]) {

            if (key == 'title') {
                var par = document.createElement('p');
                par.setAttribute('class', 'article__title');
                var node = document.createTextNode('Название: ' + objectData.items[i][key]);
                par.appendChild(node);
                main.appendChild(par);
            } else if (key == 'price') {
                var _par = document.createElement('p');
                _par.setAttribute('class', 'article__price');
                var _node = document.createTextNode('Цена: ' + objectData.items[i][key] + ' руб.');
                _par.appendChild(_node);
                main.appendChild(_par);
            } else if (key == 'image') {
                var img = document.createElement('img');
                img.setAttribute('class', 'article__img');
                img.setAttribute('src', objectData.items[i][key]);
                main.appendChild(img);
            } else if (key == 'category') {
                var _par2 = document.createElement('p');
                _par2.setAttribute('class', 'article__category');
                var _node2 = document.createTextNode('Категория: ' + objectData.items[i][key].title);
                _par2.appendChild(_node2);
                main.appendChild(_par2);
            }
        }
    }
};

btn[0].addEventListener('click', function () {
    httpGetAsync(shiny.method, shiny.url, function (data) {
        toggleNode();
        addData(data);
    });
});

btn[1].addEventListener('click', function () {
    httpGetAsync(diski.method, diski.url, function (data) {
        toggleNode();
        addData(data);
    });
});